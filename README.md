#Directed Study in Computer History

#### Possible Things to Do

* Readings of important books (see 'bookrecs')
* Visiting museums and places of historical significance
* Doing writeups on books and museums
* Learning older languages, such as COBOL or Fortran
* Assembling or otherwise utilizing older hardware